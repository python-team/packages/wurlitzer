Source: wurlitzer
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Alexandre Marie <alexandre.marie@synchrotron-soleil.fr>,
           Picca Frédéric-Emmanuel <picca@debian.org>,
           Julian Gilbey <jdg@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-pytest <!nocheck>,
               python3-setuptools
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/python-team/packages/wurlitzer.git
Homepage: https://github.com/minrk/wurlitzer
Vcs-Browser: https://salsa.debian.org/python-team/packages/wurlitzer
Rules-Requires-Root: no

Package: python3-wurlitzer
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: Capture C-level output in context managers
 A common task in Python (especially while testing or
 debugging) is to redirect sys.stdout to a stream or a file while
 executing some piece of code. However, simply "redirecting stdout" is
 sometimes not as easy as one would expect. In particular, things
 become interesting when you want C code running within your Python
 process (including, but not limited to, Python modules implemented as
 C extensions) to also have its stdout redirected according to your
 wish. This turns out to be tricky and leads us into the interesting
 world of file descriptors, buffers and system calls.
 .
 This package supports redirecting this output in a straightforward way
 using a context manager.
